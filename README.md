# Snap Interview Test - Bank API

## How to run
before send requests you must do the following :

first check copy .env.example to .env file
then change database configs 
if you are using sqlite , then put an absolute path in DB_DATABASE

then run following commands:
```
composer install
php artisan migrate --seed
php key:generate
```

with below command the application is ready to use

`php artisan serve`

## Available apis
Please note that below request  headers are required:
```
Accept: application/json
Content-Type: application/json
```

1.Transfer

* address : /api/v1/account/transfer
* method : Post
* input: {from:"",to:"",amount:""}
* available cards for from and to = "6219-8619-0912-1393","6037-9972-4878-4838",
  "6219-8610-3184-2148",
  "5022-2913-9661-4191",


2.Report
* address : /api/v1/report
* method : Get
