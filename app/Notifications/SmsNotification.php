<?php
namespace App\Notifications;
use App\Broadcasting\SmsChannel;
use App\Sms\SmsServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class SmsNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $phoneNumber;
    private $message;
    private $smsService;

    public function __construct($phoneNumber, $message, SmsServiceInterface $smsService)
    {
        $this->phoneNumber = $phoneNumber;
        $this->message = $message;
        $this->smsService = $smsService;
    }

    public function via($notifiable)
    {
        return SmsChannel::class;
    }

    public function toSms($notifiable)
    {
        return [
            'phone' => $this->phoneNumber,
            'message' => $this->message,
        ];
    }

}
