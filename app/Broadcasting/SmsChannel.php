<?php

namespace App\Broadcasting;

use App\Models\User;
use App\Notifications\SmsNotification;
use App\Sms\SmsServiceInterface;

class SmsChannel
{
    /**
     * Create a new channel instance.
     */
    public function __construct(SmsServiceInterface $smsService)
    {
        $this->smsService = $smsService;
    }

    /**
     * Authenticate the user's access to the channel.
     */
    public function join(User $user): array|bool
    {
        return true;
    }

    public function send(object $notifiable, SmsNotification $notification)
    {
        $smsData = $notification->toSms($notifiable);

        return $this->smsService->sendSms($smsData['phone'], $smsData['message']);
    }
}
