<?php

namespace App\Sms;
interface SmsServiceInterface
{
    public function sendSms($phoneNumber, $message);
}
