<?php
namespace App\Sms;
interface SmsProviderInterface
{
    public function sendSms($phoneNumber, $message);
}
