<?php

namespace App\Sms;

use Ghasedak\Laravel\GhasedakFacade;
use Illuminate\Support\Facades\Log;

class GhasedakSmsProvider implements SmsProviderInterface
{
    public function sendSms($phoneNumber, $message)
    {
        try {
            $sender = "200020";
            $response = GhasedakFacade::SendSimple($phoneNumber, $message, $lineNumber = $sender);
            Log::info($response);
            return true;
        } catch (\Exceptions $ex) {
            Log::error($ex->getMessage());
            return false;
        }
    }

}
