<?php

namespace App\Sms;
class SmsService implements SmsServiceInterface
{
    private $smsProvider;

    public function __construct(SmsProviderInterface $smsProvider)
    {
        $this->smsProvider = $smsProvider;
    }

    public function sendSms($phoneNumber, $message)
    {
        try {
            // Send SMS using the SMS provider
            $result = $this->smsProvider->sendSms($phoneNumber, $message);
            // Check the result and handle any errors
            if ($result) {
                // SMS sent successfully
                return 'SMS sent successfully';
            } else {
                // Handle SMS sending failure
                return 'Failed to send SMS';
            }
        } catch (\Exception $e) {
            // Handle any exceptions or errors that occurred during SMS sending
            return 'Error: ' . $e->getMessage();
        }
    }
}
