<?php

namespace App\Sms;
use Illuminate\Support\Facades\Log;
use Kavenegar;

class KavehnegarSmsProvider implements SmsProviderInterface
{
    public function sendSms($phoneNumber, $message)
    {
        try{
            $sender = "200010";		//This is the Sender number
            $receptor = array($phoneNumber);			//Receptors numbers

            $result = Kavenegar::Send($sender,$receptor,$message);
            Log::info($result);
        }
        catch(\Kavenegar\Exceptions\ApiException $e){
            Log::error($e->errorMessage());
            return false;
        }
        catch(\Kavenegar\Exceptions\HttpException $e){
            Log::error($e->errorMessage());
            return false;
        }catch(\Exceptions $ex){
            Log::error($ex->getMessage());
            return false;
}
    }
}
