<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TransactionFee extends Model
{
    use HasFactory;

    protected $fillable=[
        'fee'
    ];
    function transaction(): BelongsTo{
        return $this->belongsTo(Transaction::class);
    }
}
