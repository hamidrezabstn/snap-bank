<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use  HasFactory, Notifiable;

    /**
     * Get the comments for the blog post.
     */
    public function accounts(): HasMany
    {
        return $this->hasMany(Account::class);
    }

//    public function cards(): HasManyThrough
//    {
//        return $this->hasManyThrough(Card::class,Account::class);
//    }
}
