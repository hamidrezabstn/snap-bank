<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Transaction extends Model
{
    use HasFactory;

    function from(): BelongsTo{
        return $this->belongsTo(Card::class,'from_card_id');
    }

    function to(): BelongsTo{
        return $this->belongsTo(Card::class,'to_card_id');
    }

    function fee(): HasOne{
        return $this->hasOne(TransactionFee::class);
    }
}
