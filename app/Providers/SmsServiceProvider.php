<?php

namespace App\Providers;

use App\Sms\KavehnegarSmsProvider;
use App\Sms\SmsProviderInterface;
use App\Sms\SmsService;
use App\Sms\SmsServiceInterface;
use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        $this->app->bind(SmsServiceInterface::class, SmsService::class);
        $this->app->bind(SmsProviderInterface::class, KavehnegarSmsProvider::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
