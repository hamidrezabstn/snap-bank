<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransferRequest;
use App\Models\Card;
use App\Models\Transaction;
use App\Notifications\SmsNotification;
use App\Rules\CreditCard;
use App\Sms\SmsServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ReportController extends Controller
{

    function index(){
        $startTime = Carbon::now()->subMinutes(10);

        $topUsers = Transaction::join('cards', 'transactions.from_card_id', '=', 'cards.id')
            ->join('accounts', 'cards.account_id', '=', 'accounts.id')
            ->join('users', 'accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.first_name','users.last_name','users.phone', \DB::raw('COUNT(*) as transaction_count'))
            ->where('transactions.created_at', '>=', $startTime)
            ->groupBy('users.id', 'users.first_name')
            ->orderByDesc('transaction_count')
            ->take(3)
            ->get();

        $result = [];

        foreach ($topUsers as $user) {
            $transactions = Transaction::join('cards', 'transactions.from_card_id', '=', 'cards.id')
                ->join('accounts', 'cards.account_id', '=', 'accounts.id')
                ->join('users', 'accounts.user_id', '=', 'users.id')
                ->select('transactions.*')
                ->where('users.id', $user->id)
                ->orderBy('transactions.created_at', 'desc')
                ->take(10)
                ->get();

            $result[$user->id] = [
                'id' => $user->id,
                'name' => $user->first_name." ".$user->last_name,
                'phone' => $user->phone,
                'transaction_count' => $user->transaction_count,
                'transactions' => $transactions,
            ];
        }

        return ['top_users' => $result];
    }


}
