<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransferRequest;
use App\Models\Card;
use App\Models\Transaction;
use App\Notifications\SmsNotification;
use App\Rules\CreditCard;
use App\Sms\SmsServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    private int $TRANSACTION_FEE=500;
    private SmsServiceInterface $smsService;

    public function __construct(SmsServiceInterface $smsService)
    {
        $this->smsService = $smsService;
    }

    function transfer(TransferRequest $request){

        $validated = $request->validated();

        $from_card = Card::firstWhere(['card_number'=>$validated['from']]);
        if(!$from_card){
            return ["error"=>"from card number is not available "];
        }

        $from_account = $from_card->account;

        if(!$from_account){
            return ["error"=>"from account is not available "];
        }

        $to_card = Card::firstWhere(['card_number'=>$validated['to']]);

        if(!$to_card){
            return ["error"=>"to card number is not available "];
        }

        $to_account = $to_card->account;

        if(!$to_account){
            return ["error"=>"to account is not available "];
        }

        if($from_account->balance === $validated['amount']){
            return ["error"=>"From account can not provide transfer amount plus fee"];
        }

        if($from_account->balance < $validated['amount'] + $this->TRANSACTION_FEE ){
            return ["error"=>"From account can not provide transfer amount plus fee"];
        }

        DB::transaction(function() use ($from_card, $to_card, $to_account, $validated, $from_account) {
            $from_account->balance -= $validated['amount'] + $this->TRANSACTION_FEE;
            $from_account->save();
            $to_account->balance = $validated['amount'];
            $to_account->save();

            $transaction = new Transaction();

            $transaction->from_card_id = $from_card->id;
            $transaction->to_card_id = $to_card->id;
            $transaction->amount = $validated['amount'];
            $transaction->save();
            $transaction->fee()->create(["fee"=>$this->TRANSACTION_FEE]);


        });
        //todo send sms on transaction finished
        $from_sms_tpl = env("FROM_SMS_TPL","Your balance is decreased by :amount\nNew balance is :balance");
        $from_sms = str_replace([":amount",":balance"],[$validated['amount'],$from_account->balance],$from_sms_tpl);

        $to_sms_tpl = env("TO_SMS_TPL","Your balance is increased by :amount\nNew balance is :balance");
        $to_sms = str_replace([":amount",":balance"],[$validated['amount'],$to_account->balance],$to_sms_tpl);

        $from_account->user->notify(new SmsNotification($from_account->user->phone,$from_sms,$this->smsService));
        $to_account->user->notify(new SmsNotification($to_account->user->phone,$to_sms,$this->smsService));

        return ["success"=>$validated['amount']." is successfully transferred"];
    }
    function report(){
        $startTime = Carbon::now()->subMinutes(10);

        $topUsers = Transaction::join('cards', 'transactions.from_card_id', '=', 'cards.id')
            ->join('accounts', 'cards.account_id', '=', 'accounts.id')
            ->join('users', 'accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.first_name','users.last_name','users.phone', \DB::raw('COUNT(*) as transaction_count'))
            ->where('transactions.created_at', '>=', $startTime)
            ->groupBy('users.id', 'users.first_name')
            ->orderByDesc('transaction_count')
            ->take(3)
            ->get();

        $result = [];

        foreach ($topUsers as $user) {
            $transactions = Transaction::join('cards', 'transactions.from_card_id', '=', 'cards.id')
                ->join('accounts', 'cards.account_id', '=', 'accounts.id')
                ->join('users', 'accounts.user_id', '=', 'users.id')
                ->select('transactions.*')
                ->where('users.id', $user->id)
                ->orderBy('transactions.created_at', 'desc')
                ->take(10)
                ->get();

            $result[$user->id] = [
                'id' => $user->id,
                'name' => $user->first_name." ".$user->last_name,
                'phone' => $user->phone,
                'transaction_count' => $user->transaction_count,
                'transactions' => $transactions,
            ];
        }

        return ['top_users' => $result];
    }


}
