<?php

namespace App\Http\Requests;

use App\Rules\CreditCard;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class TransferRequest extends FormRequest
{
    protected $redirect='';
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function expectsJson(): bool
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'from' => $this->sanitizeCardNumber($this->from),
            'to' => $this->sanitizeCardNumber($this->to),
        ]);
    }

    private function sanitizeCardNumber($cardnumber): array|string|null
    {
            // Mapping array for Arabic and Persian numbers to English numbers
            $numberMap = array(
                '٠' => '0',
                '١' => '1',
                '٢' => '2',
                '٣' => '3',
                '٤' => '4',
                '٥' => '5',
                '٦' => '6',
                '٧' => '7',
                '٨' => '8',
                '٩' => '9',
                '۰' => '0',
                '۱' => '1',
                '۲' => '2',
                '۳' => '3',
                '۴' => '4',
                '۵' => '5',
                '۶' => '6',
                '۷' => '7',
                '۸' => '8',
                '۹' => '9'
            );

            // Regular expression pattern to match Arabic and Persian numbers
            $pattern = '/[\x{0660}-\x{0669}\x{06F0}-\x{06F9}\x{FB50}-\x{FBB9}\x{06CC}]/u';

            // Replace Arabic and Persian numbers with English numbers
            $replacedString = preg_replace_callback($pattern, function ($matches) use ($numberMap) {
                return $numberMap[$matches[0]];
            }, $cardnumber);

            return $replacedString;
        }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'from' => ['required',new CreditCard()],
            'to' => ['required',new CreditCard()],
            'amount' => 'required|numeric|min:1000|max:50000000',
        ];
    }

    public function messages(): array
    {
        return [
            'from.required' => 'A from is required',
            'to.required' => 'A to is required',
            'amount.required' => 'A amount is required',
            'amount.min' => 'Minimum amount value is 1000',
            'amount.max' => 'Maximum amount value is 5000000',
        ];
    }
}
