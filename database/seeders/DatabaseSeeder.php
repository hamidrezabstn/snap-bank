<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Account;
use App\Models\Card;
use App\Models\Transaction;
use App\Models\TransactionFee;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    private $availableCards = [
        "6219-8619-0912-1393",
        "6037-9972-4878-4838",
        "6219-8610-3184-2148",
        "5022-2913-9661-4191",
        "6219-8610-2738-9468",
    ];
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()
            ->count(2)
            ->has(Account::factory()->count(2)
                ->has(Card::factory()->count(1)->state(new Sequence(
                    fn(Sequence $sequence) => ['card_number' => $this->availableCards[$sequence->index]],
                ))))
            ->create();
        Transaction::factory()
            ->count(5)
            ->state(new Sequence(
                fn(Sequence $sequence) => [
                    'from_card_id' => fake()->numberBetween(1,2),
                    'to_card_id' => fake()->numberBetween(2,3),
                    'amount' => fake()->numberBetween(20000,50000)
                ],
            ))->create();
        TransactionFee::factory()
            ->count(5)
            ->state(new Sequence(
                fn(Sequence $sequence) => [
                    'transaction_id' => $sequence->index+1,
                    'fee' => 500,
                ],
            ))->create();
    }
}
